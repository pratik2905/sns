﻿namespace Znode.Multifront.PaymentApplication.Api
{
    public struct PaymentConstants
    {
        public const string EnableSwagger = "EnableSwagger";
        public const string SwaggerBuildVersion = "SwaggerBuildVersion";
        public const string CORS_Domains = "CORS_Domains";
        public const string ZnodePrivateKey = "ZnodePrivateKey";
        public const string Znode_PrivateKey = "Znode-PrivateKey";        
    }
}