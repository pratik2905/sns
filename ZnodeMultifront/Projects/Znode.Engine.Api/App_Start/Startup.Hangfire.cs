﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using Hangfire;
using Hangfire.SqlServer;
using Znode.Api.Core;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api
{
    public partial class Startup
    {
        private IEnumerable<IDisposable> GetHangfireServers()
        {
            GlobalConfiguration.Configuration
                .UseLogProvider(new NoLoggingProvider())
                .UseSqlServerStorage(ConfigurationManager.ConnectionStrings["ZnodeHangfireDB"].ConnectionString, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(Convert.ToInt32(ZnodeApiSettings.HangfireCommandBatchMaxTimeout)),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(Convert.ToInt32(ZnodeApiSettings.HangfireSlidingInvisibilityTimeout)),
                    QueuePollInterval = TimeSpan.FromMilliseconds(Convert.ToInt32(ZnodeApiSettings.HangfireQueuePollInterval)),
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true,
                    //This will disable auto generation of hangfire db.
                    PrepareSchemaIfNecessary = Convert.ToBoolean(ZnodeApiSettings.HangfirePrepareSchemaIfNecessary),
                });

            yield return new BackgroundJobServer(
                new BackgroundJobServerOptions { ServerName = $"{Environment.MachineName}:{Process.GetCurrentProcess().Id}:{AppDomain.CurrentDomain.Id}" });
        }
    }
}