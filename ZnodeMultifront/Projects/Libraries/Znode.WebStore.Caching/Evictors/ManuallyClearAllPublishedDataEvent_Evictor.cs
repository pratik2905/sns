﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Caching;
using Znode.Libraries.Caching.Events;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Caching.Core;

namespace Znode.WebStore.Caching.Evictors
{
    internal class ManuallyClearAllPublishedDataEvent_Evictor : BaseWebStoreEvictor<ManuallyClearAllPublishedDataEvent>
    {
        protected override void Setup(ManuallyClearAllPublishedDataEvent cacheHtmlCacheEvent)
        {

        }

        protected override void EvictNonDictionaryCacheData(ManuallyClearAllPublishedDataEvent cacheHtmlCacheEvent)
        {
            ClearHtmlCache();
        }

        protected override List<string> EvictSpecificDictionaryCacheKeys(ManuallyClearAllPublishedDataEvent cacheHtmlCacheEvent)
        {
            return new List<string>();
        }

        protected override bool IsDictionaryItemStale(ManuallyClearAllPublishedDataEvent cacheHtmlCacheEvent, string key)
        {
            return true;
        }

        protected override void Teardown(ManuallyClearAllPublishedDataEvent cacheHtmlCacheEvent)
        {

        }
    }
}
