﻿class WidgetTemplate extends ZnodeBase {
    constructor() {
        super();
    }

    
    DeleteWidgetTemplate(control): any {
        var widgetTemplateId = DynamicGrid.prototype.GetMultipleSelectedIds();
        var fileName = DynamicGrid.prototype.GetMultipleValuesOfGridColumn('File Name');
        if (widgetTemplateId.length > 0) {
            Endpoint.prototype.DeleteWidgetTemplate(widgetTemplateId, fileName, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    ValidateTemplateCode(): boolean {
        var isValid = true;
        var widgetTemplateId = $('#WidgetTemplateId').val()

        if (widgetTemplateId < 1 && $("#Code").val() !='') {
            Endpoint.prototype.IsWidgetTemplateExist($("#Code").val(), function (response) {
                if (response.data) {
                    $("#Code").addClass("input-validation-error");
                    $("#valCode").addClass("error-msg");
                    $("#valCode").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistWidgetTemplate"));
                    $("#valCode").show();
                    isValid = false;
                    ZnodeBase.prototype.HideLoader();
                }
            });
        }
        return isValid;
    }
}